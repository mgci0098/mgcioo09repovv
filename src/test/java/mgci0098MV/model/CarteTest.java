package mgci0098MV.model;

import org.junit.*;

import java.util.Date;

import static org.junit.Assert.*;

public class CarteTest {

    private Carte c1, c2, c3;

    @BeforeClass
    public static void setUp() throws Exception {
        System.out.println("Before all class Carte");
    }

    @Before
    public void setup() {

        c1 = new Carte();
        c2 = new Carte();
        c2.setTitlu("Ion");
        c1.setAnAparitie("2010");

//        c2.getCarteFromString("Ion;Liviu Rebreanu;2018;Humanitas;mostenire,pamant,Ana");
        System.out.println("setup succes");
    }

    @After
    public void teardown() {
        c1 = null;
        c2 = null;
        System.out.println("teardown");
    }

    @Test
    public void test_getTitlu() {
        assertEquals("egale", "Ion", c2.getTitlu());
        assertNotEquals("diferite", "Ion", c1.getTitlu());
        System.out.println("getTitlu succes");
    }

    @Ignore("testul getTitlu2 este in lucru")
    //adaugare de Ignore sare peste tastare la rularea totala , dar afiseaza la rularea individuala de testare
    @Test(expected = NullPointerException.class)
    public void test_getTitlu2() {
        System.out.println("a intrat in getTitlu2");                    //aici a intrat doar pe testare locala, pe testare globala nu mai intra
        assertEquals("egale", "Ion", c3.getTitlu());
        System.out.println("getTitlu2 succes");
    }


    @AfterClass
    public static void tearDown() throws Exception {
        System.out.println("After all class Carte");
    }

    @Ignore("Ciclic poate sa dea erori\n")
    @Test(timeout = 20)
    public void test_cicluInfinit() {
        int i = 0;
        while (i < 1000) System.out.println(i += 3);
        System.out.println("\nTimeOut succes");
    }


    //Modele de testare

    @Test
    public void test_getAnAparitie() {
        assertTrue("An aparitie este valid", Integer.parseInt(c1.getAnAparitie()) < 2018);
        System.out.println("getAnAparitie succes");

        c1.setAnAparitie("2035");
        assertEquals("Imposibil", false, Integer.parseInt(c1.getAnAparitie()) < 2018);
        System.out.println("Valoare an introdusa gresit");
    }


    @Test(timeout = 30)
    public void test_cautaDupaAutor(){
        c1.adaugaAutor("Vasile");
        c2.adaugaAutor("Maria");
        c3 = new Carte(); c3.adaugaAutor("Mihai");

        assertFalse(c1.cautaDupaAutor("Ion"));
        System.out.println("Cautare disponibila");
    }

}