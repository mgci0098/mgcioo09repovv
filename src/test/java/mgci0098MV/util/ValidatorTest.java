package mgci0098MV.util;

import org.junit.*;

import java.rmi.UnexpectedException;

import static org.junit.Assert.*;

public class ValidatorTest {
    String nr;
    String cuvant;
    Validator val = new Validator();

    @BeforeClass
    public static void setUpAll() {
        System.out.println("Setup before all subsequent tests Validator");
    }

    @Before
    public void setUp() throws Exception {
        nr = "2018";
        cuvant = "laborator";
        System.out.println("succes");
    }

    @After
    public void tearDown() throws Exception {
        nr = null;
        cuvant = null;
        System.out.println("teardown");
    }

    @AfterClass
    public static void tearDownAll() throws Exception {
        System.out.println("Tearing all down");
    }


    @Test
    public void isStringOK() throws Exception {
        assertTrue("este cuvant",val.isStringOK(cuvant));
        System.out.println("cuvant validat cu succes");
    }

    @Test (expected = Exception.class)
    public void isStringOKEXP() throws Exception {
        cuvant = "laborator1";
        System.out.println("se testeaza cuvantul " + cuvant);
        assertFalse("NU contine doar litere",val.isStringOK(cuvant));
    }

    @Test
    public void isNumberOK() throws Exception {
        nr = "anul2020";
        assertFalse("NU contine doar numere",val.isNumber(nr));
        System.out.println("numar cu cifre validat cu succes");
    }

    @Test
    public void isNumber() throws Exception {
        nr = "90108";
        assertTrue(val.isNumber(nr));
        System.out.println("numar validat cu succes");
    }

}