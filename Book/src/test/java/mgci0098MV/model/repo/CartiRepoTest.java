package mgci0098MV.model.repo;

import mgci0098MV.model.Carte;
import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class CartiRepoTest {

    Carte c1;
    CartiRepo repo;

    @BeforeClass
    public static void setUpAll() {
        System.out.println("Setup before all subsequent tests CArtiRepo");
    }

    @Before
    public void setUp() throws Exception {
        c1 = new Carte();
        c1.setTitlu("Laborator");
        c1.adaugaAutor("Dorian");
        c1.setAnAparitie("2018");
        c1.setEditura("UBB");
        c1.adaugaCuvantCheie("verificare");
        c1.adaugaCuvantCheie("validare");
        c1.adaugaCuvantCheie("testare");

        repo = new CartiRepo();
        System.out.println("setup succes");
    }

    @After
    public void tearDown() throws Exception {
        c1 = null;
        System.out.println("teardown");
    }

    @AfterClass
    public static void tearDownAll() throws Exception {
        System.out.println("Tearing all down");
    }

    @Test
    public void adaugaCarte() throws Exception {
        System.out.println(repo.getCarti().size());
        repo.adaugaCarte(c1);
        System.out.println(repo.getCarti().size());
        assertEquals("nume autor", "UBB", repo.cautaCarte("Dorian").get(0).getEditura());
        System.out.println("adauga carte verificat");
    }

    @Ignore("Ignorare test - Capacitare depasita")
    @Test(timeout = 20)
    public void getCarti() throws Exception {
        repo.adaugaCarte(c1);
        assertTrue("capacitate", repo.getCarti().size() < 10);
        System.out.println("Capacitate suficienta");
    }


    @Test
    public void BBT_TCs() throws Exception {

        Carte carte1 = new Carte();


    }
}