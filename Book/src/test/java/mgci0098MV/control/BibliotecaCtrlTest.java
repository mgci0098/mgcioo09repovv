package mgci0098MV.control;

import mgci0098MV.model.Carte;
import mgci0098MV.model.repo.CartiRepo;
import mgci0098MV.repository.repoInterfaces.CartiRepoInterface;
import mgci0098MV.repository.repoMock.CartiRepoMock;
import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {

    Carte c1, c2, c3, c4;
    CartiRepoInterface cartiRepo;
    BibliotecaCtrl bibliotecaCtrl;


    @BeforeClass
    public static void setUpAll() {
        System.out.println("Setup before all subsequent tests !");
    }

    @Before
    public void setUp() throws Exception {

        c1 = new Carte();
        c1.setTitlu("Calin");
        c1.adaugaAutor("EminescuTC");
        c1.setAnAparitie("1877");
        c1.setEditura("Corint");
        c1.setCuvinteCheie(new ArrayList<String>(Arrays.asList("poem", "poezie")));

        c2 = new Carte();
        c2.setTitlu("Cicoare");
        c2.adaugaAutor("CosbucTC");
        c2.setAnAparitie("2020");
        c2.setEditura("Humanitas");
        c2.setCuvinteCheie(new ArrayList<String>(Arrays.asList("poezie", "proza")));

        c3 = new Carte();
        c3.setTitlu("TheBook");
        c3.adaugaAutor("BacoviaTC");
        c3.setAnAparitie("1753");
        c3.setEditura("Socrate");
        c3.setCuvinteCheie(new ArrayList<String>(Arrays.asList("versuri", "proza")));

        c4 = new Carte();
        c4.setTitlu("Casa");
        c4.adaugaAutor("MirceaTC");
        c4.setAnAparitie("1895");
        c4.setEditura("Corint");
        c4.setCuvinteCheie(new ArrayList<String>(Arrays.asList("poezie", "nuvele")));

        cartiRepo = new CartiRepoMock();
        bibliotecaCtrl = new BibliotecaCtrl(cartiRepo);
        System.out.println("setup succes");

    }

    @After
    public void tearDown() throws Exception {
        c1 = null;
        c2 = null;
        c3 = null;
        c4 = null;
    }

    @AfterClass
    public static void tearDownAll() throws Exception {
        System.out.println("Tearing all down");
    }


    @Test
    public void adaugaCarte1() throws Exception {

        //Observatii
        //in Validator la validateCarte, primul if nu testeaza corect.
        //in CartiRepoMock exista o eroare de cautare dupa autori, la verificare nu se cauta in lista de autori, ci in lista de cuvinte cheie.
        bibliotecaCtrl.adaugaCarte(c1);
        List<Carte> carti = bibliotecaCtrl.cautaCarte("EminescuTC");
        if (carti.size() != 0) {
            System.out.println(carti.get(0).toString());
            assertEquals("name", "Calin", carti.get(0).getTitlu().contains("Calin"));
            System.out.println("Adaugare carte 1");
        }
        System.out.println("Eroare la Adaugare carte 1");
    }

    @Test (expected = Exception.class)
    public void adaugaCarte2() throws Exception {
        bibliotecaCtrl.adaugaCarte(c2);
        System.out.println("Eroare la Adaugare carte 2");

    }

    @Test
    public void adaugaCarte3() throws Exception {
        bibliotecaCtrl.adaugaCarte(c3);
        System.out.println("Adaugare carte 3");

    }

    @Test (expected = Exception.class)
    public void adaugaCarte4() throws Exception {
        bibliotecaCtrl.adaugaCarte(c4);
        System.out.println("Adaugare carte 4");

    }


}